import styled from "styled-components";

export const StyledListProduct = styled.div`
    .all-product {
        margin-top: 16px;
    }
    .pagination {
        margin-top: 24px;
        text-align: right;
    }
`