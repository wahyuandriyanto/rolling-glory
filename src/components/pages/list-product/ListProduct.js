import React, { useState, useEffect, useCallback } from 'react'

import ProductFilter from 'components/organisms/list-product/ProductFilter'
import ProductSort from 'components/organisms/list-product/ProductSort'
import CardProduct from 'components/organisms/list-product/CardProduct'
import Container from 'components/atoms/container/Container'
import qs from 'query-string'
import _ from 'lodash'
import { useHistory, useLocation } from 'react-router-dom'
import { Col, Pagination, Row, Spin } from 'antd'
import { useQuery } from 'hooks/useQuery'
import { apiCall } from 'utils/api'

import { StyledListProduct } from './Styled'


const ListProduct = () => {
    const history  = useHistory();
    const location = useLocation();
    const query    = useQuery();
    const pageNumber = query.get('page');
    const [dataProduct, setDataProduct] = useState([]);
    const [loading, setLoading]         = useState(true);

    const onPaginationChange = (page) => {
        const params = qs.parse(location.search)
        const newParams = {...params, page: page}
        history.push({
            pathname: '/',
            search: qs.stringify(newParams)
        })
    }

    const getListProduct = useCallback(() => {
        setLoading(true)
        apiCall.get('gifts', {
            params: {
                'page[number]'  : pageNumber,
                'page[size]': 6
            }
        })
        .then((res) => {
            setDataProduct(res.data)
            setLoading(false)
        })
        .catch((err) => {
            console.log(err);
        })
    }, [pageNumber])

    useEffect(() => {
        getListProduct()
    }, [getListProduct])

    let filteredDataProduct = dataProduct.data;
    
    const filterData = () => {
        const sortBy = query.get('sortBy');
        const filter = query.get('filter');
        const combinedFilter = filter?.split(',');
        const isAvailable = _.includes(combinedFilter, "available");
        const isRateHigherFrom = _.includes(combinedFilter, "4");

        if (isAvailable) {
            filteredDataProduct = _.filter(filteredDataProduct, 
                (
                    function(o) {
                        return o.attributes.stock > 0
                    }
                ))
        } 
        if (isRateHigherFrom) {
            filteredDataProduct = _.filter(filteredDataProduct, 
                (
                    function(o) {
                        return o.attributes.rating > 4
                    }
                ))
        }
        if (sortBy === 'rate') {
            filteredDataProduct = _.orderBy(filteredDataProduct, 
                [
                    function(o) { 
                        return o.attributes.rating 
                    }
                ], 
                ['desc'])
        }
    };

    filterData();
    return (
        <Spin spinning={loading}>
            <StyledListProduct>
                <Container>
                    <Row gutter={[40, 40]}>
                        <Col xs={24} md={6}>
                            <ProductFilter />
                        </Col>
                        <Col xs={24} md={18}>
                            <ProductSort />
                            {!loading && (
                                <div className="all-product">
                                    <Row gutter={[16, 16]}>
                                        {filteredDataProduct.map((item, i) => (
                                            <Col xs={24} md={8} key={i}>
                                                <CardProduct data={item.attributes} />
                                            </Col>
                                        ))}
                                    </Row>
                                </div>
                            )}
                            <div className="pagination">
                                <Pagination 
                                    current={dataProduct?.meta?.currentPage} 
                                    onChange={onPaginationChange} 
                                    total={dataProduct?.meta?.totalItems}
                                    pageSize={6} 
                                />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </StyledListProduct>
        </Spin>
    )
}

export default ListProduct