import React, { useEffect, useState } from 'react'
import { Breadcrumb, Button, Col, InputNumber, Rate, Row, Space, Spin, Tabs, Typography } from 'antd'
import { dataProductMapper } from 'components/organisms/list-product/dataProductMapper'
import { PlusOutlined, MinusOutlined, HeartFilled } from '@ant-design/icons'
import { Link, useParams } from 'react-router-dom'
import { toAbsoluteUrl } from 'utils/helpers'
import { apiCall } from 'utils/api'

import { StyledProductDetail } from './Styled'

const { Title, Text } = Typography
const { TabPane } = Tabs;

const ProductDetail = () => {
    let { id } = useParams();

    const [loading, setLoading] = useState(false);
    const [dataProduct, setDataProduct] = useState({});
    const [qty, setQty] = useState(1);


    useEffect(() => {
        setLoading(true);
        apiCall.get(`/gifts/${id}`)
        .then((res) => {
            setDataProduct(dataProductMapper(res.data.data.attributes))
            setLoading(false)
        })
        .catch((err) => {
            console.log(err);
        })
    }, [id])
   
    return (
        <Spin spinning={loading}>
            <StyledProductDetail>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <Link to="/">List Product</Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        {dataProduct.productName}
                    </Breadcrumb.Item>
                </Breadcrumb>
                <div className="product-header">
                    <Row gutter={{ xs: 0, md:54 }}>
                        <Col xs={24} md={12}>
                            <div className="product-header__image">
                                <img src={dataProduct.productImage} alt="product" />
                                {dataProduct.productBadge !== null && (
                                    <div className="image-badge">
                                        <img src={dataProduct.productBadge} alt="product badge" />
                                    </div>
                                )}
                            </div>
                        </Col>
                        <Col xs={24} md={12}>
                            <Title level={3}>
                                {dataProduct.productName}
                            </Title>
                            <div className="product-header__rating">
                                <Space size={8}>
                                    <Rate allowHalf disabled value={dataProduct.productRating} />
                                    <Text type="secondary">{dataProduct.productReview} reviews</Text>
                                </Space>
                            </div>
                            <div className="product-header__points">
                                <Space size={16}>
                                    <img src={toAbsoluteUrl('/media/icons/ic_point_large.png')} alt="point" />
                                    <Title level={4} type="success" style={{ margin: 0 }}>{dataProduct.productPoints} points</Title>
                                    <Text type="success">{dataProduct.stockLabel}</Text>
                                </Space>
                            </div>
                            <div className="product-header__desc">
                                <p dangerouslySetInnerHTML={{__html: dataProduct.productInfo}} />
                            </div>
                            <div className="product-header__qty">
                                <Text type="secondary">Jumlah</Text>
                                <div className="qty-counter">
                                    <Button 
                                        icon={<MinusOutlined style={{ color: '#525F7F' }}/>}
                                        onClick={() => setQty(qty - 1)} 
                                    />
                                    <InputNumber min={1} defaultValue={qty} value={qty} onChange={(e) => setQty(e.target.value)} />
                                    <Button 
                                        icon={<PlusOutlined style={{ color: '#525F7F' }}/>}
                                        onClick={() => setQty(qty + 1)} 
                                    />
                                </div>
                            </div>
                            <Space size={16}>
                                <Button 
                                    shape="round"
                                    icon={<HeartFilled style={{ color: '#ffffff' }}/>}
                                    type="danger"
                                    size="large"
                                />
                                <Button type="primary" shape="round" size="large">
                                    Reedem
                                </Button>
                                <Button 
                                    type="primary" 
                                    shape="round"
                                    size="large" 
                                    style={{ backgroundColor: '#ffffff', color: '#79B625' }}
                                >
                                    Add to cart
                                </Button> 
                            </Space>
                        </Col>
                    </Row>
                </div>
                <div className="product-body">
                    <Tabs defaultActiveKey={1}>
                        <TabPane tab="Info produk" key={1}>
                            <Title level={4} type="success">
                                Rincian
                            </Title>
                            <p dangerouslySetInnerHTML={{__html: dataProduct.productDesc}} />
                        </TabPane>
                    </Tabs>
                </div>
            </StyledProductDetail>
        </Spin>    
    )
}

export default ProductDetail