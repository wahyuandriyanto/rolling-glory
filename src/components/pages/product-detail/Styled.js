import styled from "styled-components";

export const StyledProductDetail = styled.div`
    .product-header {
        margin-top: 54px;
        &__image {
            padding: 6% 14% 9%;
            position: relative;
            .image-badge {
                position: absolute;
                top: 0;
                right: 0;
            }
        }
        &__rating {
            margin-bottom: 8px;
        }
        &__points,
        &__desc {
            margin-bottom: 16px;
        }
        &__qty {
            margin-bottom: 32px;
            .qty-counter {
                margin-top: 8px;
                .ant-btn-default {
                    background: #D8D8D8;
                }
                .ant-input-number {
                    border: none;
                    width: 40px;
                    &-input {
                        text-align: center;
                    }
                    &-handler-wrap {
                        display: none;
                    }
                }
            }
        }
    }
    .product-body {
        .ant-tabs-content {
            margin-top: 24px;
            p {
                line-height: 56px;
            }
        }
    }
`