import styled from 'styled-components';

export const StyledProductFilter = styled.div`
    .filter-header {
        padding-bottom: 16px;
        border-bottom: 1px solid #D5D5D5;
        display: flex;
        justify-content: space-between;
        &__title {
            font-size: 17px;
            font-weight: 600;
        }
        &__sort {
            display: flex;
            align-items: center;
            gap: 16px;
            span {
                color: #888888;
            }
            .ant-select-selector {
                border: 1px solid #9D9D9D;
                border-radius: 33px;
                padding: 0 24px;
            }
        }
    }
    .filter-box {
        margin-top: 16px;
        border: 1px solid #D8D8D8;
        padding: 16px;
        border-radius: 4px;
        label {
            margin-bottom: 12px;
        }
        span {
            color: #888888;
        }
    }
`

export const StyledCardProduct = styled.div`
    border: 1px solid #D8D8D8;
    padding: 32px;
    border-radius: 8px;
    position: relative;
    .product-badge {
        position: absolute;
        top: -6px;
        right: -7px;
        max-height: 80px;
        img {
            min-height: 80px;
            max-height: 80px;
        }
    }
    .product-stock {
        position: relative;
        font-size: 12px;
        font-weight: 600;
        z-index: 999;
    }
    .product-thumbnail {
        min-height: 275px;
        max-height: 275px;
        display: flex;
        align-items: center;
        margin: 16px 0;
        img {
            max-height: 275px;
        }
    }
    .product-rate {
        .ant-rate {
            font-size: 11px;
            &-star {
                :not(:last-child) {
                    margin-right: 3px;
                }
            }
        }
    }
    .product-wishlist {
        position: relative;
        z-index: 999;
        .ant-btn {
            position: absolute;
            right: -18px;
            bottom: 3px;
            &-default {
                background: transparent;
                border: 1px solid #D8D8D8;
            }
        }
    }
    .product-overlay {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background: rgba(116, 183, 27, .95);
        border-radius: 8px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        padding: 24px;
        opacity: 0;
        visibility: hidden;
        transition: visibility 0s, opacity .3s linear;
        z-index: 9;
        &__cta {
            width: 100%;
            margin-top: 24px;
            .ant-btn {
                background: transparent;
                border: 1px solid #ffffff;
                color: #ffffff;
                width: 100%;
                :hover {
                    color: #ffffff;
                }
            }
        }
    }
    :hover {
        .product-badge {
            display: none;
            
        }
        .product-overlay {
            opacity: 1;
            visibility: visible;
        }
        .product-stock span {
            color: #ffffff !important;
        }
    }
`