import React from 'react'

import { EyeOutlined, HeartOutlined, HeartFilled } from '@ant-design/icons'
import { dataProductMapper } from './dataProductMapper'
import { Button, Rate, Space, Typography } from 'antd'
import { useHistory } from 'react-router-dom'
import { toAbsoluteUrl } from 'utils/helpers'
import { apiCall } from 'utils/api'

import { StyledCardProduct } from './Styled'


const { Title, Text } = Typography;

const CardProduct = ({ data }) => {
    const history = useHistory();
    let item = dataProductMapper(data);

    const handleWishlist = (id) => {
        apiCall.post(`gifts/${id}/wishlist`)
        .then((res) => {
            console.log(res);
        })
    }

    return (
        <StyledCardProduct>
            <div className="product-badge">
                {item.productBadge !== null && (
                    <img src={item.productBadge} alt="product badge" />
                )}
            </div>
            <div className="product-stock">
                <Text type={item.stockType}>
                    {item.stockLabel}
                </Text>
            </div>
            <div className="product-thumbnail">
                <img src={item.productImage} alt="product" />
            </div>
            <div className="product-title">
                <Title level={5}>
                    {item.productName}
                </Title>
            </div>
            <Space size={4}>
                <img src={toAbsoluteUrl('/media/icons/ic_point.png')} alt="point" />
                <Text type="success">{item.productPoints} points</Text>
            </Space>
            <div className="product-rate">
                <Space size={8}>
                    <Rate allowHalf disabled value={item.productRating} />
                    <Text type="secondary">{item.productReview} reviews</Text>
                </Space>
            </div>
            <div className="product-wishlist">
                {item.isProductWishlist ? (
                    <Button 
                        shape="round"
                        icon={<HeartFilled style={{ color: '#ffffff' }}/>}
                        type="danger"
                        onClick={() => handleWishlist(item.productId)}
                    />
                ):(
                    <Button 
                        shape="round"
                        icon={<HeartOutlined style={{ color: '#D8D8D8' }}/>}
                        onClick={() => handleWishlist(item.productId)}
                    />
                )}      
            </div>
            <div className="product-overlay">
                <Title level={5} style={{ color: '#ffffff' }}>
                    {item.productName}
                </Title>
                <div className="product-overlay__cta">
                    <Button 
                        shape="round" 
                        icon={<EyeOutlined style={{ color: '#ffffff' }} />}
                        onClick={() => history.push(`/product/${item.productId}`)}
                    >
                        View detail
                    </Button>
                </div>
            </div>    
        </StyledCardProduct>
    )
}

export default CardProduct