import React from 'react'
import qs from 'query-string'

import { useHistory, useLocation } from 'react-router-dom'
import { useQuery } from 'hooks/useQuery'
import { Checkbox, Row, Col } from 'antd'

import { StyledProductFilter } from './Styled'


const ProductFilter = () => {
    const location = useLocation();
    const history  = useHistory();
    const query    = useQuery();
    const filter = query.get('filter');
    const combinedFilter = filter?.split(',');

    const handleCheckboxChange = (value) => {
        const params = qs.parse(location.search)
        const newParams = {...params, filter: value.toString()}
        history.push({
            pathname: '/',
            search: qs.stringify(newParams)
        })
    }
    return (
        <StyledProductFilter>
            <div className="filter-header">
                <div className="filter-header__title">
                    Filter
                </div>
            </div>
            <div className="filter-box">
                <Checkbox.Group onChange={handleCheckboxChange} defaultValue={combinedFilter} style={{ width: '100%' }}>
                    <label>
                        <Row>
                            <Col span={20}>
                                <span>Rating 4 ke atas</span>
                            </Col>
                            <Col span={4}>
                                <Checkbox value="4" />
                            </Col>
                        </Row>  
                    </label>
                    <label>
                        <Row>
                            <Col span={20}>
                                <span>Stok Tersedia</span>
                            </Col>
                            <Col span={4}>
                                <Checkbox value="available" />
                            </Col>
                        </Row>
                    </label>
                </Checkbox.Group>
            </div>
        </StyledProductFilter>
    )
}

export default ProductFilter