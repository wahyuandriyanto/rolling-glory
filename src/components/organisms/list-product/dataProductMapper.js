export function dataProductMapper(data) {
    const getStockStatus = (stock) => {
        if (stock >= 5) {
            return {
                label: 'In Stock',
                type : 'success'
            }
        } else if (stock < 5 && stock > 0) {
            return {
                label: 'Stock < 5',
                type : 'danger'
            }
        } else {
            return {
                label: 'Sold Out',
                type : 'danger'
            }
        }
    }

    const getProductBadge = (data) => {
        let status;
        if (data.isNew === 1) {
            status = "new"
        } 
        if (data.rating >= 4 && data.numOfReviews > 25) {
            status = "best-seller"
        } 
        if (data.rating >= 4 && data.numOfReviews > 25 && data.isNew === 1) {
            status = "hot-item"
        }
        switch (status) {
            case "new":
                return '/media/icons/ic_new.png'
            case "best-seller":
                return '/media/icons/ic_best_seller.png'
            case "hot-item":
                return '/media/icons/ic_hot.png'     
            default:
                return null;
        }
    }
    return {
        'productId'     : data.id,
        'productBadge'  : getProductBadge(data),
        'stockLabel'    : getStockStatus(data.stock).label,
        'stockType'     : getStockStatus(data.stock).type,
        'productImage'  : data.images[0],
        'productName'   : data.name,
        'productInfo'   : data?.info,
        'productDesc'   : data?.description,
        'productPoints' : data.points,
        'productRating' : data.rating,
        'productReview' : data.numOfReviews,
        'isProductWishlist': data.isWishlist === 1 ? true : false

    }
}