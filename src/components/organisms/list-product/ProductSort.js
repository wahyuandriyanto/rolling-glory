import React from 'react'
import qs from 'query-string'
import { useHistory, useLocation } from 'react-router-dom'
import { useQuery } from 'hooks/useQuery'
import { Select } from 'antd'

import { StyledProductFilter } from './Styled'

const { Option } = Select;

const ProductSort = () => {

    const history = useHistory();
    const location = useLocation();
    const query = useQuery();
    const sortBy = query.get('sortBy');

    const handleSortChange = (value) => {
        const params = qs.parse(location.search)
        const newParams = {...params, sortBy: value}
        history.push({
            pathname: '/',
            search: qs.stringify(newParams)
        })
    }

    return (
        <StyledProductFilter>
            <div className="filter-header">
                <div className="filter-header__title">
                    Product List
                </div>
                <div className="filter-header__sort">
                    <span>Urutkan</span>
                    <Select defaultValue={sortBy === null ? "newProduct" : sortBy} onChange={handleSortChange}>
                        <Option value="newProduct">Terbaru</Option>
                        <Option value="rate">Rating</Option>
                    </Select>
                </div>
            </div>
        </StyledProductFilter>
    )
}

export default ProductSort