import React from 'react';
import Container from 'components/atoms/container/Container';
import { toAbsoluteUrl } from 'utils/helpers';
import { Link } from 'react-router-dom';

import { StyledHeader } from './Styled';


const Header = () => {
    return (
        <StyledHeader>
            <Container>
                <Link to="/">
                    <div className="nav-brand">
                        <img src={toAbsoluteUrl('/media/logo/logo.png')} alt="logo" />
                    </div>
                </Link>
            </Container>
        </StyledHeader>
    )
}

export default Header