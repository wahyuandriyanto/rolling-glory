import styled from 'styled-components';

export const StyledHeader = styled.div`
    position: fixed;
    top: 0;
    width: 100%;
    background: #ffffff;
    z-index: 9999;
    border-bottom: 1px solid rgb(232, 232, 232);
`