import React from 'react'
import Container from 'components/atoms/container/Container'
import { toAbsoluteUrl } from 'utils/helpers'
import { Link } from 'react-router-dom'
import { Col, Row, Space } from 'antd'

import { StyledFooter } from './Styled'


const Footer = () => {
    return (
        <StyledFooter>
            <Container>
                <Row gutter={[16, 16]}>
                    <Col xs={24} md={12}>
                        <Space size={32}>
                            <Link to="#">
                                <img src={toAbsoluteUrl('/media/icons/ic_ig.png')} alt="instagram" />
                            </Link>
                            <Link to="#">
                                <img src={toAbsoluteUrl('/media/icons/ic_fb.png')} alt="facebook" />
                            </Link>
                            <Link to="#">
                                <img src={toAbsoluteUrl('/media/icons/ic_twitter.png')} alt="twitter" />
                            </Link>
                        </Space>
                    </Col>
                    <Col xs={24} md={12}>
                        <div className="copyright">
                            <Link to="#">
                                Terms & Condition
                            </Link>
                            <span>
                                Copyright © 2018. All rights reserved. PT Radya Gita Bahagi
                            </span>
                        </div>
                    </Col>
                </Row>
            </Container>
        </StyledFooter>
    )
}

export default Footer