import styled from "styled-components";

export const StyledFooter = styled.div`
    background: #232323;
    padding: 32px 0;
    .copyright {
        font-size: 14px;
        display: flex;
        align-items: center;
        a, span {
            color: #ffffff;
            padding: 0px 16px;
        }
        a {
            border-right: 1px solid #ffffff;
        }
        @media (max-width: 768px){
            flex-wrap: wrap;
            a, span {
                padding: 4px 0;
            }
            a {
                border-right: none;
            }
        }
    }
    @media (max-width: 768px){
        padding: 16px 0;
    }
`