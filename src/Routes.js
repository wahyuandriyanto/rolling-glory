import React, { lazy, Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'

const ListProduct   = lazy(() => import('components/pages/list-product/ListProduct'))
const ProductDetail = lazy(() => import('components/pages/product-detail/ProductDetail'))


export function Routes() {
    return (
        <Suspense fallback="">
            <Switch>
                <Route exact path="/" component={ListProduct} />
                <Route path="/product/:id" component={ProductDetail} />
            </Switch>
        </Suspense>
    );
}

