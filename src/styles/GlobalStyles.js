import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
    :root {
        --primary-color       : #74B71B;
    }
    * {
        box-sizing: border-box;
    }
    body {
        font-family: 'Raleway', sans-serif;
    }
    img {
        max-width: 100%;
        height: auto;
    }

`

export default GlobalStyle