import React from 'react'
import MainThemes from 'themes/MainThemes';
import { BrowserRouter } from 'react-router-dom'
import { Routes } from 'Routes'

export default function App({ basename }) {
    return (
        <BrowserRouter basename={basename}>
            <MainThemes>
                <Routes />
            </MainThemes>
        </BrowserRouter> 
    );
}
