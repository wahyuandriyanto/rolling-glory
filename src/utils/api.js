import axios from 'axios'

const apiCall = axios.create({
    baseURL: process.env.REACT_APP_BASE_API_URL,
    headers: {
        'Content-Type': 'application/json',
    },
});

export { apiCall }