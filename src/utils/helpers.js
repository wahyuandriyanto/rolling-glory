export const toAbsoluteUrl = pathname => process.env.PUBLIC_URL + pathname;

export const round = (value, step) => {
    step || (step = 1.0);
    var inv = 1.0 / step;
    return Math.round(value * inv) / inv;
}
