import React from 'react'
import Container from 'components/atoms/container/Container'
import Header from 'components/molecules/header/Header'
import Footer from 'components/molecules/footer/Footer'
import styled from 'styled-components'

const StyledAppContent = styled.div`
    padding: 120px 0;
    min-height: 91vh;
    @media (max-width: 768px){
        padding: 91px 0;
    }
`

const MainThemes = ({ children }) => {
    return (
        <>
            <Header />
            <StyledAppContent>
                <Container>
                    {children}
                </Container>
            </StyledAppContent>
            <Footer />
        </>
    )
}

export default MainThemes